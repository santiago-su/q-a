import React, { Suspense } from 'react';
import Container from '@material-ui/core/Container';
import { Loading } from '../components';
import { useStyles } from './styles';

export const AsyncComponent = Component => {
  const classes = useStyles();
  return props => {
    return (
      <Suspense fallback={<Loading />}>
        <Container className={classes.root} maxWidth="md">
          <Component {...props} />
        </Container>
      </Suspense>
    );
  };
};
