import actionTypes from '../constants';

export const questionActions = {
  addQuestionRequest: () => ({
    type: actionTypes.addQuestionRequest
  }),
  addQuestionSuccess: payload => ({
    type: actionTypes.addQuestionSuccess,
    payload
  }),
  addQuestionFailure: payload => ({
    type: actionTypes.addQuestionFailure,
    payload
  }),
  removeQuestion: payload => ({
    type: actionTypes.removeQuestion,
    payload
  }),
  removeAllQuestions: () => ({
    type: actionTypes.removeAllQuestions
  }),
  sortAlphabetically: () => ({
    type: actionTypes.sortAlphabetically
  })
};
