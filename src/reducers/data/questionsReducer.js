import uuid from 'uuidv4';
import { dissoc, without } from 'ramda';
import actionTypes from '../../constants';

const initialId = uuid();
const initialState = {
  allIds: [initialId],
  byId: {
    [initialId]: {
      question: 'This is a test question',
      answer: 'Some test answer',
      id: initialId
    }
  },
  sorted: true
};

const removeById = (id, state) => {
  const byId = dissoc(id, state.byId);
  const allIds = without([id], state.allIds);
  return { byId, allIds };
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.sortAlphabetically:
      return { ...state, sorted: !state.sorted };
    case actionTypes.addQuestionRequest:
      return {
        ...state,
        loading: true,
        error: false,
        success: false
      };
    case actionTypes.addQuestionSuccess:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.id]: {
            question: action.payload.question,
            answer: action.payload.answer,
            id: action.payload.id
          }
        },
        allIds: [...state.allIds, action.payload.id],
        loading: false,
        error: false,
        success: true
      };
    case actionTypes.addQuestionFailure:
      return {
        ...state,
        errorMessage: action.payload,
        loading: false,
        error: true,
        success: false
      };
    case actionTypes.removeQuestion:
      return removeById(action.payload, state);
    case actionTypes.removeAllQuestions:
      return { allIds: [], byId: {} };
    default:
      return state;
  }
};
