import { combineReducers } from 'redux';
import questionsReducer from './questionsReducer';

const dataReducer = combineReducers({
  questions: questionsReducer
});

export default dataReducer;
