import { createMuiTheme } from '@material-ui/core/styles';

export const lightTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#222222'
    },
    secondary: {
      main: '#004440'
    }
  }
});

export const darkTheme = createMuiTheme({
  palette: {
    type: 'dark'
  }
});
