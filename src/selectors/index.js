import { createSelector } from 'reselect';
import { sortBy, prop, compose, toLower, reverse } from 'ramda';

export const getQuestions = state => state.data.questions;
export const isSorted = state => state.data.questions.sorted;
export const isLoading = state => state.data.questions.loading;

export const getQuestionsList = createSelector(
  state => getQuestions(state),
  state => isSorted(state),
  (list, sorted) => {
    switch (sorted) {
      case true:
        return sortBy(
          compose(
            toLower,
            prop('question')
          ),
          Object.values(list.byId)
        );
      default:
        return reverse(
          sortBy(
            compose(
              toLower,
              prop('question')
            ),
            Object.values(list.byId)
          )
        );
    }
  }
);
