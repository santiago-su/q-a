export const addQuestionRequest = 'ADD_QUESTION_REQUEST';
export const addQuestionSuccess = 'ADD_QUESTION_SUCCESS';
export const addQuestionFailure = 'ADD_QUESTION_FAILURE';
export const removeQuestion = 'REMOVE_QUESTION';
export const removeAllQuestions = 'REMOVE_ALL_QUESTIONS';
export const sortAlphabetically = 'TOGGLE_SORT';
