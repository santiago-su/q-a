import React, { lazy } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { AsyncComponent } from './routes';
import { lightTheme } from './themes';

const QuestionsPage = lazy(() =>
  import(/* webpackChunkName: "Questions" */ './pages/Questions')
);

const App = () => {
  const Questions = AsyncComponent(QuestionsPage);
  return (
    <MuiThemeProvider theme={lightTheme}>
      <CssBaseline />
      <Questions />
    </MuiThemeProvider>
  );
};

export default App;
