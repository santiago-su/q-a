import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SortByAlpha from '@material-ui/icons/SortByAlpha';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useSelector, useDispatch } from 'react-redux';
import {
  QuestionDialog,
  DeleteConfirmationDialog,
  useDialogs
} from '../Dialog';
import Tooltip from '../Tooltip';
import { getQuestionsList } from '../../selectors';
import { questionActions } from '../../actions';
import { useStyles } from './styles';

const QuestionList = ({ addQuestion }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const questions = useSelector(getQuestionsList);
  const {
    editableQuestion,
    deletableQuestion,
    openQuestionDialog,
    openDeleteQuestionDialog,
    handleOpenQuestionEditDialog,
    handleOpenDeleteQuestionDialog,
    handleCloseDeleteQuestionDialog,
    handleCloseQuestionDialog
  } = useDialogs();

  const confirmDelete = question =>
    dispatch(questionActions.removeQuestion(question.id));

  const toggleSort = () => dispatch(questionActions.sortAlphabetically());

  return (
    <div className={classes.root}>
      <Tooltip message="Sort alphabetically" position="top">
        <IconButton aria-label="sort" onClick={() => toggleSort()}>
          <SortByAlpha />
        </IconButton>
      </Tooltip>
      {questions.map(question => (
        <div className={classes.questionPanel} key={question.id}>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls={question.id}
              id={question.id}
            >
              <Tooltip message="Click to see answer" position="top">
                <Typography className={classes.heading}>
                  {question.question}
                </Typography>
              </Tooltip>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.questionDetails}>
              <Typography>{question.answer}</Typography>
              <div className={classes.questionDetailActions}>
                <Button
                  color="secondary"
                  variant="contained"
                  onClick={() => handleOpenQuestionEditDialog(question)}
                >
                  Edit
                </Button>
                <Button
                  color="primary"
                  onClick={() => handleOpenDeleteQuestionDialog(question)}
                >
                  Delete
                </Button>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          {editableQuestion && (
            <QuestionDialog
              question={editableQuestion}
              openDialog={openQuestionDialog}
              handleCloseDialog={handleCloseQuestionDialog}
              onSubmit={addQuestion}
            />
          )}
          {deletableQuestion && (
            <DeleteConfirmationDialog
              openDelete={openDeleteQuestionDialog}
              item={deletableQuestion}
              handleCloseDeleteDialog={handleCloseDeleteQuestionDialog}
              onSubmit={confirmDelete}
            />
          )}
        </div>
      ))}
    </div>
  );
};

QuestionList.propTypes = {
  addQuestion: PropTypes.func
};

export default QuestionList;
