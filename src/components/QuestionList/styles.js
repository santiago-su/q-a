import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  },
  questionPanel: {
    margin: theme.spacing(2)
  },
  questionDetails: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  questionDetailActions: {
    alignSelf: 'end'
  }
}));
