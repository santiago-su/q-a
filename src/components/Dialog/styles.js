import { makeStyles } from '@material-ui/core/styles';

export const questionStyles = makeStyles(() => ({
  questionsContainer: {
    display: 'flex',
    flexDirection: 'column'
  }
}));
