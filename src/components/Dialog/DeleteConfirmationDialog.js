import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { Typography } from '@material-ui/core';

const DeleteConfirmationDialog = ({
  openDelete,
  handleCloseDeleteDialog,
  item,
  onSubmit
}) => {
  const handleDelete = i => () => {
    onSubmit(i);
    handleCloseDeleteDialog();
  };
  return (
    <>
      <Dialog
        data-testid="test-delete-dialog"
        open={openDelete}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <Typography>
            Are you sure you want to delete {item.question}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            Disagree
          </Button>
          <Button
            data-testid="test-delete-dialog-submit"
            onClick={handleDelete(item)}
            color="primary"
            autoFocus
          >
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

DeleteConfirmationDialog.propTypes = {
  openDelete: PropTypes.bool.isRequired,
  handleCloseDeleteDialog: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default DeleteConfirmationDialog;
