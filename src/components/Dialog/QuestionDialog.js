import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import { questionStyles } from './styles';

const QuestionDialog = ({
  question,
  openDialog,
  handleCloseDialog,
  onSubmit
}) => {
  const classes = questionStyles();
  const [state, setState] = useState({});

  useEffect(() => {
    setState(question);
  }, [question]);

  const handleSubmit = () => {
    onSubmit(state);
    handleCloseDialog();
  };

  return (
    <>
      <Dialog
        fullWidth
        maxWidth="lg"
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Ask a Question</DialogTitle>
        <DialogContent>
          <div className={classes.questionsContainer}>
            <TextField
              label="Question"
              name="question"
              InputProps={{
                inputProps: { 'data-testid': 'question-field' }
              }}
              value={state.question}
              onChange={event =>
                setState({ ...state, question: event.target.value })
              }
              margin="normal"
              variant="outlined"
            />
            <TextField
              fullWidth
              multiline
              label="Answer"
              name="answer"
              InputProps={{
                inputProps: { 'data-testid': 'answer-field' }
              }}
              value={state.answer}
              onChange={event =>
                setState({ ...state, answer: event.target.value })
              }
              margin="normal"
              variant="outlined"
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            CANCEL
          </Button>
          <Button color="primary" onClick={handleSubmit}>
            ADD
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

QuestionDialog.propTypes = {
  question: PropTypes.shape({
    question: PropTypes.string,
    answer: PropTypes.string
  }),
  openDialog: PropTypes.bool.isRequired,
  handleCloseDialog: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default QuestionDialog;
