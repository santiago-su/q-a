import { useState } from 'react';

const useDialogs = () => {
  const [editableQuestion, setEditableQuestion] = useState({});
  const [openQuestionDialog, setOpenQuestionDialog] = useState(false);
  const [deletableQuestion, setDeletableQuestion] = useState({});
  const [openDeleteQuestionDialog, setOpenDeleteQuestionDialog] = useState(
    false
  );

  const handleOpenDeleteQuestionDialog = question => {
    setDeletableQuestion(question);
    setOpenDeleteQuestionDialog(true);
  };

  const handleCloseDeleteQuestionDialog = () => {
    setOpenDeleteQuestionDialog(false);
  };

  const handleOpenQuestionEditDialog = question => {
    setEditableQuestion(question);
    setOpenQuestionDialog(true);
  };

  const handleCloseQuestionDialog = () => {
    setOpenQuestionDialog(false);
  };

  return {
    editableQuestion,
    openQuestionDialog,
    handleOpenQuestionEditDialog,
    setOpenQuestionDialog,
    handleCloseQuestionDialog,
    deletableQuestion,
    openDeleteQuestionDialog,
    handleOpenDeleteQuestionDialog,
    handleCloseDeleteQuestionDialog
  };
};

export default useDialogs;
