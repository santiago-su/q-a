import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Tooltip = ({ message, position, children }) => {
  const [display, setDisplay] = useState(false);
  const hideTooltip = () => setDisplay(false);
  const showTooltip = () => setDisplay(true);

  return (
    <span className="tooltip" onMouseLeave={hideTooltip}>
      {display && (
        <div className={`tooltipContainer tooltip-${position}`}>
          <div className="tooltipMessage">{message}</div>
        </div>
      )}
      <span
        className="showTooltip"
        onFocus={showTooltip}
        onMouseOver={showTooltip}
      >
        {children}
      </span>
    </span>
  );
};

Tooltip.propTypes = {
  message: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default Tooltip;
