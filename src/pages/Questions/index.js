import React from 'react';
import uuid from 'uuidv4';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useDispatch, useSelector } from 'react-redux';
import { QuestionDialog, useDialogs } from '../../components/Dialog';
import { QuestionList, Loading } from '../../components';
import { isLoading } from '../../selectors';
import { questionActions } from '../../actions';
import { useStyles } from './styles';

const asyncFunc = () => {
  return new Promise(res => {
    setTimeout(() => res(), 5000);
  });
};

const Questions = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const [fakeNetwork, setFakeNetwork] = React.useState(false);
  const {
    openQuestionDialog,
    setOpenQuestionDialog,
    handleCloseQuestionDialog
  } = useDialogs();

  const addQuestion = async question => {
    if (fakeNetwork) {
      dispatch(questionActions.addQuestionRequest());
      try {
        await asyncFunc();
        dispatch(questionActions.addQuestionSuccess(question));
      } catch (error) {
        dispatch(questionActions.addQuestionFailure(error));
      }
    } else {
      dispatch(questionActions.addQuestionSuccess(question));
    }
  };

  const handleCheckNetwork = event => setFakeNetwork(event.target.checked);

  return (
    <>
      <Paper className={classes.actionsContainer}>
        <Typography variant="h4">Q&A</Typography>
        <FormControlLabel
          label="Simulate slow network"
          control={
            <Checkbox
              checked={fakeNetwork}
              onChange={handleCheckNetwork}
              value={fakeNetwork}
              inputProps={{
                'aria-label': 'primary checkbox'
              }}
            />
          }
        />
        <Button color="secondary" onClick={() => setOpenQuestionDialog(true)}>
          + NEW QUESTION
        </Button>
        <Button
          color="primary"
          onClick={() => dispatch(questionActions.removeAllQuestions())}
        >
          REMOVE ALL QUESTIONS
        </Button>
      </Paper>
      {loading ? <Loading /> : <QuestionList addQuestion={addQuestion} />}
      <QuestionDialog
        question={{ question: '', answer: '', id: uuid() }}
        openDialog={openQuestionDialog}
        handleCloseDialog={handleCloseQuestionDialog}
        onSubmit={addQuestion}
      />
    </>
  );
};

export default Questions;
