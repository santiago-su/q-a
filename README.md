# Q&A

This is an example application using React, Redux and other
libraries.

#### Installation
Before running this you need to have installed.
- [Node](https://nodejs.org/en/): 12.8.1
- [NPM](https://nodejs.org/en/): 6.10.2

Clone the repo
```bash
git clone https://github.com/santiago-su/q-a
```
Install dependencies
```bash
npm install
```

#### Usage
1. Run project with `npm start`.
2. Go to `localhost:3000` and play around.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
